# helloworld_travis_demo

[![Build Status](https://travis-ci.org/rutujar/helloworld_travis_demo.svg?branch=master)](https://travis-ci.org/rutujar/helloworld_travis_demo)  [![CircleCI](https://circleci.com/gh/rutujar/helloworld_travis_demo.svg?style=svg)](https://circleci.com/gh/rutujar/helloworld_travis_demo)   [![Build status](https://ci.appveyor.com/api/projects/status/2wp50shl25yfw19q?svg=true)](https://ci.appveyor.com/project/rutujar/helloworld-travis-demo)   [![codecov](https://codecov.io/gh/rutujar/helloworld_travis_demo/branch/master/graph/badge.svg)](https://codecov.io/gh/rutujar/helloworld_travis_demo)

This is a demo project implemented various Continous Integration tools using Travis, Jenkins,Appveyor,Circle and so on.

# To run and test my project
1. Fork the document 
2. open travis website, enable the repository
3. build the project


